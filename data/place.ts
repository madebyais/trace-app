interface IPlace {
  name: string
  capacity: number
}

const Place: Array<IPlace> = [
  { name: "Jakarta", capacity: 12000 }
]

export default Place