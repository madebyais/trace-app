import dynamic from "next/dynamic"
import { FC } from "react"
// @ts-ignore
const ReactQRReader = dynamic(() => import("modern-react-qr-reader"), {ssr: false})

interface IQRReaderProps {
  isShow: boolean
  setIsShow: Function
  onQRScan?: Function
  onQRError?: Function
}

const QRReader: FC<IQRReaderProps> = ({ isShow, setIsShow, onQRScan, onQRError }) => (
  <div>
    {isShow &&
      <div>
        {// @ts-ignore
          <ReactQRReader delay={300} onError={onQRError} onScan={onQRScan} style={{width: '100%', height: '100vh'}} />
        }
      </div>
    }
  </div>
)

export default QRReader