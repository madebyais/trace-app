import Head from "next/head";
import Image from "next/image";
import Link from "next/link";

const LayoutHeader = ({ title, backUrl, children }: any) => (
  <div>
    <Head>
      <title>Peduli Lindungi</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </Head>

    <main className="w-full h-screen">
      <div className="w-full flex items-center space-x-5 py-3 px-2 border-b border-b-gray">
        <div className="pt-1"><Link href={backUrl}><Image src={"/image-page/btn-back.png"} width={20} height={20} /></Link></div>
        <div className="text-lg">{title}</div>
      </div>

      <div className="overflow-y-auto">
        {children}
      </div>
    </main>
  </div>
)

export default LayoutHeader