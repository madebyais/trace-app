import Head from "next/head"

const Layout = ({ children }: any) => (
  <div>
    <Head>
      <title>Peduli Lindungi</title>
      <meta name="viewport" content="width=device-width, initial-scale=1.0" />
    </Head>

    <main>
      {children}
    </main>
  </div>
)

export default Layout