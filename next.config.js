/** @type {import('next').NextConfig} */
let nextConfig = {
  reactStrictMode: false,
  swcMinify: true,
}

if (process.env.NODE_ENV === "production") {
  const withPWA = require("next-pwa")
  nextConfig = withPWA({
    pwa: {
      dest: "public",
      register: true,
      skipWaiting: true,
    },
    reactStrictMode: false,
    swcMinify: true,
  })
}

module.exports = nextConfig
