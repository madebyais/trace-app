import { NextPage } from "next";
import { useEffect, useState } from "react";
import LayoutHeader from "../components/layout-header";

const Sidenav: NextPage = () => {
  const [state, setState] = useState<any>({ profile: {} })

  useEffect(() => {
    if (localStorage.getItem("profile")) {
      const profile: any = JSON.parse(localStorage.getItem("profile")! || "{}")
      setState({ ...state, profile })
    }
  }, [])

  return (
    <LayoutHeader title="Vaccination Status &amp; COVID-19 T..." backUrl="/account">
      <div className="p-5">
        <div className="border rounded-lg shadow-lg">
          <div className="p-5 flex items-center space-x-5">
            <div><img src={"/image-page/vaccination-status/icon-user.png"} width={30} /></div>
            <div>{state.profile.name || ""}</div>
          </div>
          <div className="border"></div>
          <div className="p-5 flex items-center space-x-5">
            <div><img src={"/image-page/vaccination-status/icon-ktp.png"} width={30} /></div>
            <div>{state.profile.id_card || ""}</div>
          </div>
        </div>
      </div>

      <div className="p-5 py-1">
        <div className="status-green text-white text-center rounded-full font-semibold py-2">Green</div>
        <div className="mt-2"><img src={"/image-page/vaccination-status/text-info-color-status.png"} /></div>
      </div>

      <img src="/image-page/vaccination-status/separator.png" />

      <div className="flex justify-between items-center px-5 pt-2">
        <div className="flex items-center space-x-5">
          <div><img src="/image-page/vaccination-status/icon-vaccination-status.png" width={30} /></div>
          <div className="pt-1 font-semibold">Third dose vaccinated</div>
        </div>
        <div><img src="/image-page/vaccination-status/icon-green-vaccinated.png" width={30} /></div>
      </div>

      <div className="mt-2">
        <img src={"/image-page/vaccination-status/info-2.png"} />
      </div>
    </LayoutHeader>
  )
}

export default Sidenav