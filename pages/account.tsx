import { NextPage } from "next";
import Image from "next/image";
import Link from "next/link";
import { useEffect, useState } from "react";
import LayoutHeader from "../components/layout-header";

const Account: NextPage = () => {
  const [state, setState] = useState<any>({ profile: {} })

  useEffect(() => {
    if (localStorage.getItem("profile")) {
      const profile: any = JSON.parse(localStorage.getItem("profile")! || "{}")
      setState({ ...state, profile })
    }
  }, [])

  return (
    <LayoutHeader title="Account" backUrl="/home">
      <div className="p-3 flex justify-between items-center border-b border-b-grary">
        <div className="flex items-center space-x-5">
          <div><Image src={"/image-page/account/icon-user.png"} width={50} height={50} /></div>
          <div>
            <div>{state.profile.name || ""}</div>
            <div className="text-gray-600">{state.profile.phone || ""}</div>
          </div>
        </div>
        <div><img src={"/image-page/account/btn-edit.png"} width={40} /></div>
      </div>

      <img src={"/image-page/account/btn-profile-qr-code.png"} />

      <Link href={"/vaccination-status"}>
        <img src={"/image-page/account/btn-vaccination-status-and-covid-19-test-result.png"} width="100%" />
      </Link>

      <div className="pl-3">
        <div>
          <img src={"/image-page/account/btn-other.png"} />
        </div>
      </div>
    </LayoutHeader>
  )
}

export default Account