import { useEffect, useState } from 'react'
import type { NextPage } from 'next'
import Layout from '../components/layout'

import { QrReader } from 'react-qr-reader'
import Link from 'next/link'
import moment from 'moment'

const Home: NextPage = () => {
  const [state, setState] = useState<any>({ isShowQR: false, isShowCheckInResult: false, isShowFormSetting: false, profile: {} })

  useEffect(() => {
    if (localStorage.getItem("profile")) {
      const profile: any = JSON.parse(localStorage.getItem("profile")! || "{}")
      setState({ ...state, profile })
    }
  }, [])

  const onScan = (result: any, error: any) => {
    if (result && result.text) {
      setState({ ...state, isShowQR: false, isShowCheckInResult: true })
    }
  }

  const onClickShowQr = () => {
    setState({ ...state, isShowQR: true })
  }

  if (state.isShowQR) {
    return <QRContainer state={state} setState={setState} onScan={onScan} />
  }

  if (state.isShowCheckInResult) {
    return <CheckInResult state={state} setState={setState} />
  }

  if (state.isShowFormSetting) {
    return <FormSetting state={state} setState={setState} />
  }

  return (
    <Layout>
      <div className="w-full flex justify-between items-center p-5 py-2 pt-3">
        <div className="flex items-center space-x-3">
          <div><Link href="/account"><img src="/image-page/home/icon-user.png" width={40} /></Link></div>
          <div className="w-full font-semibold">Hi, <span className="underlined">{state.profile.name || ""}</span></div>
        </div>
        <div><img src="/image-page/home/icon-bell.png" width={17} onClick={(e: any) => setState({ ...state, isShowFormSetting: true })} /></div>
      </div>
      <div className="flex justify-center p-5 py-2">
        <img src="/image-page/home/icon-checkin.png" onClick={onClickShowQr} />
      </div>
      <div className="flex justify-center p-5 py-2">
        <img src="/image-page/home/icon-menu.png" />
      </div>
    </Layout>
  )
}

export default Home

const QRContainer = ({ state, setState, onScan }: any) => {
  const onClose = () => setState({ ...state, isShowQR: false })
  return (
    <div className="w-full h-screen bg-black flex flex-col justify-between">
      <div>
        <div className="flex justify-between items-center text-white p-5">
          <button className='text-white' onClick={onClose}>
            <i className="fa fa-remove" />
          </button>
          <div className="text-lg">Scan Qr Code</div>
          <div className="text-black">tes</div>
        </div>
        {// @ts-ignore 
          <QrReader constraints={{ facingMode: 'environment' }} onResult={onScan} />
        }
      </div>
      <div><img className='cursor-pointer' onClick={e => setState({ ...state, isShowQR: false, isShowCheckInResult: true })} src="/image-page/check-in/footer.png" /></div>
    </div>
  )
}

const CheckInResult = ({ state, setState }: any) => {
  const onClickClose = () => {
    setState({ ...state, isShowQr: false, isShowCheckInResult: false })
  }

  return (
    <div>
      <div className="border p-5">
        {/* <i className="fa fa-remove" onClick={onClickClose} /> */}
        <img src="/image-page/btn-close.png" width={18} onClick={onClickClose} />
      </div>
      <div className="p-10 flex justify-center">
        <img src="/image-page/check-in/icon-checkin-success.png" width={150} />
      </div>
      <div className="border shadow rounded-lg p-5 mx-10">
        <div className="flex justify-center mb-5"><img src="/image-page/check-in/icon-personal-checkin.png" width={120} /></div>
        {state.profile.location && <div className="flex justify-center mb-2 font-bold">{state.profile.location}</div>}
        {state.profile.location && state.profile.current_capacity && state.profile.max_capacity && <div className="text-sm text-gray-500 flex justify-center mb-2">
          Total Occupancy: <div className="font-semibold text-black mx-2">{state.profile.current_capacity}</div> / {state.profile.max_capacity}</div>
        }
        <div className="flex justify-center space-x-5 mb-5">
          <div className="flex items-center space-x-2">
            <div><img src="/image-page/check-in/icon-calendar.png" width={18} /></div>
            <div className="text-xs">{moment().format("DD MMM YYYY")}</div>
          </div>
          <div className="flex items-center space-x-2">
            <div><img src="/image-page/check-in/icon-time.png" width={15} /></div>
            <div className="text-xs">{moment().format("HH:mm")}</div>
          </div>
        </div>

        <div className="status-green text-white text-center text-xs py-1 rounded-full">{state.profile.name}</div>
      </div>
    </div>
  )
}

const FormSetting = ({ state, setState }: any) => {
  const onClose = () => {
    setState({ ...state, isShowFormSetting: false })
  }

  const onSave = (e: any) => {
    e.preventDefault()
    localStorage.setItem("profile", JSON.stringify(state.profile))
    onClose()
  }

  const onChange = (e: any) => {
    const key: string = e.target.name
    const value: string = e.target.value
    const profile: any = { ...state.profile, [key]: value }
    setState({ ...state, profile })
  }

  return (
    <div className="p-10 h-screen flex flex-col justify-between">
      <div className="space-y-5">
        <form onSubmit={onSave}>
          <div><input type="text" onChange={onChange} value={state.profile && state.profile.name ? state.profile.name : ""} name="name" placeholder="Name" className="p-3 border w-full" /></div>
          <div><input type="text" onChange={onChange} value={state.profile && state.profile.id_card ? state.profile.id_card : ""} name="id_card" placeholder="ID Card" className="p-3 border w-full" /></div>
          <div><input type="text" onChange={onChange} value={state.profile && state.profile.phone ? state.profile.phone : ""} name="phone" placeholder="Phone Number" className="p-3 border w-full" /></div>
          <div><input type="text" onChange={onChange} value={state.profile && state.profile.location ? state.profile.location : ""} name="location" placeholder="Location" className="p-3 border w-full" /></div>
          <div><input type="text" onChange={onChange} value={state.profile && state.profile.current_capacity ? state.profile.current_capacity : ""} name="current_capacity" placeholder="Current Capacity" className="p-3 border w-full" /></div>
          <div><input type="text" onChange={onChange} value={state.profile && state.profile.max_capacity ? state.profile.max_capacity : ""} name="max_capacity" placeholder="Max Capacity" className="p-3 border w-full" /></div>
          <button className="w-full border btn-qr-code text-black text-center p-2" type="submit">Save</button>
        </form>
      </div>
      <button className="w-full border bg-red-500 text-white text-center p-2" onClick={onClose}>Close</button>
    </div>
  )
}