import type { NextPage } from 'next'

const SplashScreen: NextPage = () => {
  const onClick = () => {
    window.location.href = "/home"
  }

  return (
    <div className="h-screen flex flex-col justify-between items-center py-5" onClick={onClick}>
      <div></div>
      <div className="px-20"><img src="/image-page/splash-screen/logo.png" /></div>
      <div className="px-5"><img src="/image-page/splash-screen/icon-footer.png" /></div>
    </div>
  )
}

export default SplashScreen
